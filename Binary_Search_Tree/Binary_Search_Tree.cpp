// Binary_Search_Tree.cpp : Defines the entry point for the console application.

//Assumption: There is a space after comma

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

struct Node {
	int r;		//current root
	Node* L;	//Left root
	Node* R;	//Right root
};

void readData(vector<int> & data, string fileName);
void inOrder(Node* traverse, vector<int> & inorder);
void preOrder(Node* head, vector<int> & preorder);
void postOrder(Node* head, vector<int> & postorder);
void insert(Node* head, vector<int> data);


int main()
{
	string userInput;
	vector<int> inorder;
	vector<int> preorder;
	vector<int> postorder;
	Node* head = new Node;
	Node* traverse = head;
	head->L = NULL;
	head->R = NULL;
	head->r = NULL;

	vector<int> data;
	ofstream output;
	output.open("output.txt");

	readData(data, "input.txt");
	insert(head, data);
	inOrder(traverse, inorder);
	preOrder(traverse, preorder);
	postOrder(traverse, postorder);

	output << "Input Data: ";
	cout << "Input Data: ";
	for (int i = 0; i < data.size(); i++)
	{
		output << data[i] << " ";
		cout << data[i] << " ";
	}

	output << endl << endl << "In-Order\tPre-Order\tPost-Order" << endl;
	cout << endl << endl << "In-Order\tPre-Order\tPost-Order" << endl;
	for (int i = 0; i < inorder.size(); i++)
	{
		output << inorder[i] << "\t\t" << preorder[i] << "\t\t" << postorder[i] << endl;
		cout << inorder[i] << "\t\t" << preorder[i] << "\t\t" << postorder[i] << endl;
	}
	
	cout << endl;
	output << endl;
	output.close();
    return 0;
}


void readData(vector<int> & data, string fileName)
{
	string userInput;
	bool found = false;

	ifstream input;
	input.open(fileName);

	while (input.good())
	{
		input >> userInput;
		for (int i = 0; i < userInput.length(); i++)
		{
			if (userInput[i] == ',')
			{
				data.push_back(stoi(userInput.substr(0, i)));
				found = true;
			}

		}
		if (!found)
		{
			data.push_back(stoi(userInput));
		}
		found = false;
	}

	input.close();
}

void insert(Node* head, vector<int> data)
{
	Node* traverse = new Node;
	
	for (int i = 0; i < data.size(); i++)		//Go through each data in the vector
	{
		traverse = head;
		if (traverse->r == NULL && traverse->L == NULL && traverse->R == NULL)	//If head node is empty...
		{
			traverse->r = data[i];		//...insert current data to head node
		}
		else if (data[i] >= traverse->r && traverse->R == NULL)	//If current node's right is empty and value is greater than current
		{
			Node* temp = new Node;
			temp->L = NULL;
			temp->R = NULL;
			temp->r = data[i];
			traverse->R = temp;
		}
		else if (data[i] >= traverse->r)	//If value is greater than current
		{
			traverse = traverse->R;
			//Get to the bottom
			while ((data[i] < traverse->r && traverse->L != NULL) || (data[i] >= traverse->r && traverse->R != NULL))
			{
				if (data[i] < traverse->r && traverse->L != NULL)
				{
					traverse = traverse->L;
				}
				else if (data[i] >= traverse->r && traverse->R != NULL)
				{
					traverse = traverse->R;
				}
			}
			//Insert in appropriate place
			if (data[i] < traverse->r && traverse->L == NULL)	//Insert left if value is smaller
			{
				Node* temp = new Node;
				temp->L = NULL;
				temp->R = NULL;
				temp->r = data[i];
				traverse->L = temp;
			}
			else if (data[i] >= traverse->r && traverse->R == NULL)		//Insert right if value is >=
			{
				Node* temp = new Node;
				temp->L = NULL;
				temp->R = NULL;
				temp->r = data[i];
				traverse->R = temp;
			}
		}
		else if (data[i] < traverse->r && traverse->L == NULL)//If current node's left is empty and value is smaller than current
		{
			Node* temp = new Node;
			temp->L = NULL;
			temp->R = NULL;
			temp->r = data[i];
			traverse->L = temp;
		}
		else if (data[i] < traverse->r)	//If value is less than current
		{
			traverse = traverse->L;
			//Get to the bottom
			while ((data[i] < traverse->r && traverse->L != NULL) || (data[i] >= traverse->r && traverse->R != NULL))
			{
				if (data[i] < traverse->r && traverse->L != NULL)
				{
					traverse = traverse->L;
				}
				else if (data[i] >= traverse->r && traverse->R != NULL)
				{
					traverse = traverse->R;
				}
			}
			//Insert in appropriate place
			if (data[i] < traverse->r && traverse->L == NULL)	//Insert left if value is <
			{
				Node* temp = new Node;
				temp->L = NULL;
				temp->R = NULL;
				temp->r = data[i];
				traverse->L = temp;
			}
			else if (data[i] >= traverse->r && traverse->R == NULL)		//Insert right if value if >=
			{
				Node* temp = new Node;
				temp->L = NULL;
				temp->R = NULL;
				temp->r = data[i];
				traverse->R = temp;
			}
		}
	}

	traverse = head;

}

void inOrder(Node* traverse, vector<int> & inorder)
{
	Node* temp = traverse;
	if (temp != NULL)
	{
		inOrder(temp->L, inorder);
		inorder.push_back(temp->r);
		inOrder(temp->R, inorder);
	}
}

void preOrder(Node* head, vector<int> & preorder)
{
	Node* temp = head;
	if (temp != NULL)
	{
		preorder.push_back(temp->r);
		preOrder(temp->L, preorder);
		preOrder(temp->R, preorder);
	}
}

void postOrder(Node* head, vector<int> & postorder)
{
	Node* temp = head;
	if (temp != NULL)
	{
		postOrder(temp->L, postorder);
		postOrder(temp->R, postorder);
		postorder.push_back(temp->r);
	}
}